from django.urls import include, path
from djgeojson.views import GeoJSONLayerView
from . import views
from . import models
from winaps import views as w_views

app_name = 'indoor'

urlpatterns = [
    path('',views.HomePageView.as_view()),
    path('indoor/',views.LandmarkView.as_view()),
    path('indoor/data',views.GeoJsonView.as_view()),
]
