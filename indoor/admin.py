# from django.contrib.gis import admin
# all other models
from django.apps import apps
from django.contrib import admin
from .models import *

models = apps.get_models()
for model in apps.get_app_config('indoor').models.values():
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
