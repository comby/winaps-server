from django.shortcuts import render
from django.views.generic import TemplateView,DetailView
from django.apps import apps
from .models import Landmark

# Create your views here.
class HomePageView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        app_models = apps.get_app_config('indoor').get_models()
        context["models"] = {model._meta.verbose_name: model.objects.all().count() for model in app_models}
        return context

class LandmarkView(TemplateView):
    template_name = 'indoor/landmark_detail.html'
    def get_context_data(self,**kwargs):
        context =  super().get_context_data(**kwargs)
        context['queryset'] = Landmark.objects.all()
        return context
   
class GeoJsonView(TemplateView):
    template_name = 'indoor/geojson.html'
    def get_context_data(self,**kwargs):
        context = super().get_context_data(*kwargs)
        context["qs"] = Landmark.objects.all()
        return context
