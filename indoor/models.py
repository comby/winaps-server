import uuid
from django.contrib.gis.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
# Create your models here.

class Building(models.Model):
    "Un batiment"
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255,blank=False)
    pass

class Floor(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    "un étage d'un batiment"
    name = models.CharField(max_length=255,blank=True)
    elevation = models.IntegerField()
    #relations
    building = models.ForeignKey(Building,on_delete=models.PROTECT)
    pass


class Emitter(models.Model):
    """Emetteur de signal quelconque (wifi ou bluetooth ici)"""
    class Meta:
        abstract = True
    pass

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    ssid = models.CharField(max_length=255) # name of network
    mac_adress = models.CharField(max_length = 17) # bssid
    # relations
    building = models.ForeignKey(Building,on_delete=models.PROTECT)
    active_flag = models.BooleanField()
    pass

class WifiEmitter(Emitter):
    """Un point d'acces Wifi """
    high_freq = models.BooleanField()
    pass

class BtEmitter(Emitter):
    "un point d'acces en bluetooth"
    namespace = models.CharField(max_length=255)
    frame_name = models.CharField(max_length=255)
    instance = models.CharField(max_length=255)
    pass

class FileMeasure(models.Model):
    """ Informations sur les fichiers de mesures récoltés"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    brand = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    write_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    lines = models.IntegerField()
    duplicates = models.IntegerField()

    #relation
    floor = models.ForeignKey(Floor,on_delete=models.PROTECT)

class Landmark(models.Model):
    "un point d'interet, ou l'on possede des fingerprint"
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    active_flag = models.BooleanField()
    pos = models.PointField()
    alt = models.IntegerField()
    tag = models.CharField(max_length=255)
    #relations
    floor = models.ForeignKey(Floor,on_delete=models.PROTECT)
    filelink = models.ForeignKey(FileMeasure,on_delete=models.PROTECT)

    @property
    def floor_num(self):
        return self.floor.elevation

    @property
    def popupContent(self):
        ret = ""
        fingerprints = self.wififingerprint_set.all()
        for fp in fingerprints:
            ret += "<b>ssid:{} bssid: {}</b></br>".format(fp.emitter.ssid,fp.emitter.mac_adress)
            ret += "min: {} max: {}mean: {} ,std: {} </br>".format(fp.min_val,
                                                                  fp.max_val,
                                                                  fp.average,
                                                                  fp.deviation)
        return ret

       
class Fingerprint(models.Model):
    class Meta:
        abstract = True
    " statistique en un Landmark pour un emetteur donné"
    hits = models.IntegerField()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    min_val = models.FloatField()
    max_val = models.FloatField()
    average = models.FloatField()
    deviation = models.FloatField()
    #relations
    landmark = models.ForeignKey(Landmark,on_delete=models.CASCADE)
    pass

class WifiFingerprint(Fingerprint):
    emitter = models.ForeignKey(WifiEmitter,on_delete=models.PROTECT)

class BtFingerprint(Fingerprint):
    emitter = models.ForeignKey(BtEmitter,on_delete=models.PROTECT)

class Measure(models.Model):
    """ Informations générérique sur une mesure"""
    class Meta:
        abstract = True
    pass
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    moment = models.DateTimeField()
    mac_adress = models.CharField(max_length=255)
    #relations
    filelink = models.ForeignKey(FileMeasure,on_delete=models.PROTECT)
    level = models.IntegerField()
    count = models.IntegerField()
    pass

class WifiMeasure(Measure):
    frequency = models.IntegerField()
    capabilities = models.CharField(max_length=255)
    ssid = models.CharField(max_length=255)
    pass

class BtMeasure(Measure):
    frame_content = models.CharField(max_length=1024)
    name = models.CharField(max_length=255)
    namespace = models.CharField(max_length=255)
    instance = models.CharField(max_length=255)
