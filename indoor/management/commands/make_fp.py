
#!/usr/bin/env python

from django.core.management.base import BaseCommand
from django.conf import settings
from django.apps import apps
from indoor.models import *
from django.db import transaction
# from django.contrib.gis.geos import GEOSGeometry

import csv
import os
BDD_DIR = "media/bdd/Base"

WIFI = {"LANDMARK_ID":"landmark_id",
        "MINIMUM":"min_val",
        "MAXIMUM":"max_val",
        "HOTSPOT_ID":"emitter_id"
        "HITS":"hits",
        "AVERAGE":"average",
        "DEVIATION":"deviation",
}

BLUETOOTH = {

}
class Command(BaseCommand):
    help = "Import a csv file to a model"

    def add_arguments(self, parser):
        parser.add_argument("--type",type=str,help="Bluetooth or Wifi ")
        parser.add_argument("--file",type=str,help="csv file for import")

    def handle(self, *args,**options):
        filename=options.get("file",None)
        with open(os.path.join(BDD_DIR,filename),'r',encoding='utf-8') as csv_file:
            reader = csv.reader((line.replace('\0','') for line in csv_file),delimiter=";")
            header = next(reader)
            with transaction.atomic():
                for row in reader:
                    for key, value in zip(header,row):
