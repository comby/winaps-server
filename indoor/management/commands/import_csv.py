
#!/usr/bin/env python

from django.core.management.base import BaseCommand
from django.conf import settings
from django.apps import apps
from indoor.models import *
from django.db import transaction
# from django.contrib.gis.geos import GEOSGeometry

import os
import csv
import json
import math
from collections import OrderedDict
import pyproj

BDD_DIR = "media/bdd/Base"
class Command(BaseCommand):
    help = "Import a csv file to a model"

    def add_arguments(self, parser):
        parser.add_argument("--map",type=str,help="json mapping header of csv and models")
        parser.add_argument("--app",type=str,help="django app name for that model")

    
    def handle(self, *args,**options):
        trad = options.get("map",None)
        with open(trad,'rb') as f:
            map_dict = json.load(f,object_pairs_hook=OrderedDict)
        for model in map_dict:
            print(model)
            obj_dict = dict()
            file_path = map_dict[model][0]
            model_dict = map_dict[model][1]
            _model = apps.get_model(options["app"],model)
            with open(os.path.join(BDD_DIR,file_path),'r', encoding='utf-8-sig') as csv_file:
                reader = csv.reader((line.replace('\0','') for line in csv_file),delimiter=";")
                header = next(reader)
                with transaction.atomic():
                    for row in reader:
                        for key,value in zip(header,row):
                            # on ne conserve que les champs désiré
                            new_key = model_dict.get(key)
                            if new_key:
                                obj_dict[new_key] = value
                            else:
                                continue
                        if obj_dict.get("lat"):
                            x = float(obj_dict.pop("lat"))
                            y = float(obj_dict.pop("long"))
                            coords = pyproj.transform(pyproj.Proj(init="epsg:3857"),
                                                      pyproj.Proj(init="epsg:4326"),
                                                      x,y,
                                                      radians=False)
                            obj_dict["pos"]= 'POINT({:.6f} {:.6f})'.format(coords[0],coords[1])
                        obj, created = _model.objects.get_or_create(**obj_dict)
                        if created:
                            obj.save()
