from django.shortcuts import render
from indoor import models as imodels
from django.views.generic import TemplateView
from django.apps import apps

def clean_get(string):
    """Fonction pour nettoyer les données reçues par GET.
    Encodage: 
    spaces  = %1
    forward slash / = %2
    semi colon ; = %3"""
    return string.replace("%3",';').replace("%2","/").replace("%1"," ")


def receive(request):
    """Reception des request POST ou GET et utilisation des données"""
    if request.method == 'GET':
        time = request.GET.get("time")
        latitude = request.GET.get("latitude")
        longitude = request.GET.get("longitude")
        ssids =  request.GET.get("ssids")
        bssids =  request.GET.get("bssids")
        powers =  request.GET.get("powers")

    if request.method == 'POST':
        text = request.POST["text"]
    context = {'time':time,
                'latitude':latitude,
                'longitude':longitude,
                'ssids':ssids,
                'bssids':bssids,
                'powers':powers}
    return render(request,'winaps/reception.html',context)

class HomePageView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        app_models = apps.get_app_config('indoor').get_models()
        context["models"] = {model._meta.verbose_name: model.objects.all().count() for model in app_models}

        return context
