"""maps app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from djgeojson.views import GeoJSONLayerView
from . import views
from . import models


app_name = 'maps'

urlpatterns = [
    path('heat/<ssid>',views.HeatMapView.as_view(),name="heatmap"),
    path('',views.NetworkView.as_view()),
    path('ssid/<ssid>',views.NetworkView.as_view(), name='ssid-map'),
    path('bssid/<bssid>',views.NetworkView.as_view(), name='bssid-map'),
    path('send/', views.receive, name='POST request'),
]
