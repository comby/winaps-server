# coding: utf-8

from django.db import IntegrityError
from .models import Mesure
from django.contrib.gis.geos import Point, fromstr
from dateutil import parser

def request_parser(time,latitude,longitude,ssids,bssids,powers):
    """Fonction qui parse les données d'une requête.
    Prend en entrée les paramètres d'une requette brutes (encodés) et en 
    fait des objects Mesure"""

    time = parser.parse(time.replace('~','/').replace('|',' '))
    latitude = float(latitude)
    longitude = float(longitude)

    ssids = ssids.split("|")
    bssids = bssids[1:len(bssids)-1].split("|")
    powers = powers[1:len(powers)-1].split("|")
    
    compteur = 0
    integ_errors = 0
    for ssid,bssid,power in zip(ssids,bssids,powers):
        try:
            location = fromstr('POINT({} {})'.format(longitude,latitude)) 
            mesure = Mesure(time=time,point=location,puissance=power,ssid=ssid,bssid=bssid)
            mesure.save()
            compteur+=1
        except IntegrityError:
            integ_errors+=1

    return compteur,integ_errors


def log_parser(path):
    """Fonction qui parse un fichier de logs.
    Prend en entrée un fichier de log généré par l'appli winaps (such corporate).
    Chaque mesure est générée en bdd par l'object Mesure.
    """

    fichier = open(path,'r',encoding="utf-8")
    lignes = fichier.readlines()
    fichier.close()

    pos_prev = (lignes[0].split(";")[1:3])

    for ligne in lignes:
        pos = (ligne.split(";")[1:3])
        if(pos != pos_prev):

            data = ligne.rstrip('\n\r').split(";")
            time = parser.parse(data[0])
            latitude = float(pos[0])
            longitude = float(pos[1])
            ssids = data[3][0:len(data[3])-1].split("|")
            bssids = data[5][1:len(data[4])-1].split(" ")
            powers = data[4][1:len(data[5])-1].split(" ")

            # point = fromstr("Point({} {})".format(longitude,latitude),srid=4326)

            for ssid,bssid,power in zip(ssids, bssids, powers):
                location = fromstr('POINT({} {})'.format(longitude,latitude)) 
                mesure = Mesure(time=time,point=location,puissance=power,ssid=ssid,bssid=bssid)
                mesure.save()

        pos_prev = pos
