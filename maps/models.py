from django.contrib.gis.db import models

class Mesure(models.Model):
    """Object représentant une mesure.
    Hérite de point pour stocker des coordonnées géographiques
    Ajoute un paramètre de temps.
    """

    time = models.DateTimeField(auto_now=False, auto_now_add=False, blank=False, help_text="Timestamp de l'instant de la prise de mesure")
    point = models.PointField(srid=4326)
    puissance = models.IntegerField(blank=False,help_text="Puissance mesurée en dB!")
    ssid = models.CharField(max_length=255,blank=True,help_text="SSID du réseau mesuré")
    bssid = models.CharField(max_length=17,help_text="BSSID du réseau au format XX:XX:XX:XX:XX:XX")

    class Meta:
        unique_together = ('time', 'point', 'bssid')
    def __str__(self):
        return 'Mesure du {}'.format(self.time)

    @property
    def popupContent(self):
        ret = ""
        ret += '<a href="/maps/ssid/{ssid}">{ssid}</a><br>'.format(ssid=self.ssid)
        ret += '<a href="/maps/bssid/{bssid}">{bssid}</a><br>'.format(bssid=self.bssid)
        ret += '{} dBm<br>'.format(self.puissance)
        return ret
