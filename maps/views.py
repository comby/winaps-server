from django.shortcuts import render, HttpResponse

from django.views.generic.base import View, TemplateView
from django.http.response import HttpResponse

from .logparser import request_parser
from .models import Mesure

class NetworkView(TemplateView):
    template_name = 'maps/index.html'
    def get_context_data(self, **kwargs):
        ssid = self.kwargs.get("ssid")
        bssid = self.kwargs.get("bssid")
        allssid = False
        if ssid:
           queryset = Mesure.objects.filter(ssid=ssid)
        elif bssid:
           queryset = Mesure.objects.filter(bssid=bssid)
        else:
           queryset = Mesure.objects.all()
           allssid = True
        return {"queryset":queryset,"allssid":allssid}
    pass
class HeatMapView(NetworkView):
    template_name = 'maps/heat.html'

    pass

   

def receive(request):
    """Reception des request POST ou GET et utilisation des données"""
    
    time = request.GET.get("time")
    latitude = request.GET.get("latitude")
    longitude = request.GET.get("longitude")
    ssids =  request.GET.get("ssids")
    bssids =  request.GET.get("bssids")
    powers =  request.GET.get("powers")
    
    nombre,duplicats = request_parser(time,latitude,longitude,ssids,bssids,powers)

    context = {'time':time,
                'latitude':latitude,
                'longitude':longitude,
                'ssids':ssids,
                'bssids':bssids,
                'powers':powers,
                'nombre':nombre,
                'duplicats':duplicats,}
    return render(request,'maps/reception.html',context)
